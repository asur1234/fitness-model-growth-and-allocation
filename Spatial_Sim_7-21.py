import random 
import sys
import math
import numpy as np
from timeit import default_timer

start = default_timer() #starts a timer

#parameters
Nc = 7 #cell domain size for cells
Nz = 7 #cell domain height for cells
d = 3 #droplet/little neighborhood size
fIJ = .5 #fraction of cells allocated in droplet 
fI = .4 #fraction/probability of cell type I
fJ = .6 #fraction/probability of cell type J
n=0 #number of cells allocated
nI=0 #number of cells of type I
nJ=0 #number of cells of type J
nB=0 #spaces outside of edges, padded spaces
t0 = 0.0 #in hours
tf = 300.0 #in hours
dt = .01 #time step
chi = .8 #competition effect
rI0 = .05 #pop I baseline growth rate per hour
rJ0 = .05 #pop J baseline growth rate per hour
rIJ = .6 #effect of J on I
rJI = .6 #effect of I on J
rII = .3 #effect of I on itself
rJJ = .3 #effect of J on itself
r = 3 #radius or grid length to check for empty spots
ri = 1 #interaction radius 



#functions
def calculate_rate(fracI,fracJ, cellType):
    #use fitness function to evaluate and return rate based on celltype (I or J)
    if cellType == 1: #celltype I
        rate = (rI0 + rIJ*(fracJ) + rII*(fracI))*(1-(chi*(fracI + fracJ))) #fitness functions 
    else: #celltype J
        rate = (rJ0 + rJI*(fracI) + rJJ*(fracJ))*(1-(chi*(fracI + fracJ)))
    return rate #returns growth rate

def Tracepath(xd,yd,xe,ye):
    xc = xd #using the variable names Elife paper used for easier comparison
    yc = yd
    
    di = np.sign(xe-xd) #+1 if daughter has to be placed to the right, otherwise -1
    dj = np.sign(ye-yd)#+1 if daughter has to be placed up, otherwise -1 
    
    #if the x or y coordinate of a empty site is the same as the growing cell, make the direction 1 as it still as to move somwhere 
    if di == 0:
        di = 1
    if dj == 0: 
        dj = 1 
        
    T = np.zeros(shape = ((abs(xd-xe)+abs(yd-ye)),2), dtype = np.int) #create a matrix with 2 columns (one for x values and y values) and neccessary rows
    ss = [(di,0), (0,dj), (di,dj)] #vector of increments or changes in the direction of the nearest site in xdir,ydir and diagonal
    cc= 0 #number of cells that need to move
    while (abs(xc-xe) + abs(yc-ye)) >= 1: #while we still have yet to reach the empty site
        cc+=1
        nn = np.add([(xc,yc),(xc,yc),(xc,yc)],ss)
        v1 = np.subtract((xe,ye),(xd,yd)) #returns an array in the form of [xe-xd,ye-yd]
        v20 = np.subtract((nn[0]),(xd,yd)) #(nn[0][0]-xd),(nn[0][1]-yd)
        v21 = np.subtract((nn[1]),(xd,yd))
        v22 = np.subtract((nn[2]),(xd,yd))    
        
        cos_thetas = {} #dictionary of indexes and the cos_thetas of each of the neighboring sites
        #find the 3 cosine thetas  by using dot product and the 2 magnitudes:(v1x*v2x + v1y*v2y)/((sqrt(v1x^2+v1y^2)) * (sqrt(v2x^2+v2y^2)))
        cos_thetas[0] = ((v1[0]*v20[0]) + (v1[1]*v20[1])) / ((math.sqrt(pow(v1[0],2)+pow(v1[1],2)))* math.sqrt(pow(v20[0],2)+pow(v20[1],2)))
        cos_thetas[1] = ((v1[0]*v21[0]) + (v1[1]*v21[1])) / ((math.sqrt(pow(v1[0],2)+pow(v1[1],2)))* math.sqrt(pow(v21[0],2)+pow(v21[1],2)))
        cos_thetas[2] = ((v1[0]*v22[0]) + (v1[1]*v22[1])) / ((math.sqrt(pow(v1[0],2)+pow(v1[1],2)))* math.sqrt(pow(v22[0],2)+pow(v22[1],2)))
        
        #Out of the 3 possible neighboring sites, we want the one that's closest along the straight line betw the growing cell (xd,yd) & the empty site (xe,ye)
        max_cos = {} #dictionary of max cos_thetas (in case there is more than 1 max)
        for i in range(3): #sees if there are any repeating mins
            if cos_thetas[i] == max(cos_thetas.values()): #finds and puts min(s) in a dictionary
                max_cos[i] = cos_thetas[i]
        
        index = random.choice(max_cos.keys()) #if there are multiple max thetas, it randomly chooses one so model is not biased. Otherwise, it chooses the only max
        xc = nn[index][0]
        yc = nn[index][1]
        T[cc-1][0] = xc #cc-1 because python starts at index 0 
        T[cc-1][1] = yc 
    
    #leave T as is and instead find the element that is the last filled on
    T = T[np.all(T != 0, axis=1)] #get rid of any rows that did not contain cells to be moved or rows that had zeros (sometimes one can reach an empty site with less steps than the T size calculated)
    return T
#----------------------------------------------------------------------------
#main

#Allocation of cells (in bottom layer)
c = np.zeros(((Nc+(2*ri), Nc+(2*ri), Nz+(2*ri)))) #intialize array of cells
CellDict = {} #creates dictionary to store location of allocated cells 

#puts -1 in spaces that are beyond the edges;pads array of cells 

for i in range(0,Nc+(2*ri)): 
    for j in range(0,ri): #bound is really ri-1, but python does not count last bound in a for loop so it is ri
        for k in range(0,Nc+(2*ri)):
            c[i,j,k]= -1
            
for i in range(0,ri): 
    for j in range(0,Nc+(2*ri)): 
        for k in range(0,Nc+(2*ri)):
            c[i,j,k]= -1

for i in range(0,Nc+(2*ri)): 
    for j in range(Nc+ri,Nc+(2*ri)): 
        for k in range(0,Nc+(2*ri)):
            c[i,j,k]= -1  

for i in range(Nc+ri,Nc+(2*ri)): 
    for j in range(0,Nc+(2*ri)): 
        for k in range(0,Nc+(2*ri)):
            c[i,j,k]= -1

for i in range(0,Nc+(2*ri)):
    for j in range(0,Nc+(2*ri)):
        for k in range(0,ri):
            c[i,j,k]=-1

for i in range(0,Nc+(2*ri)):
    for j in range(0,Nc+(2*ri)):
        for k in range(Nc+ri,Nc+(2*ri)):
            c[i,j,k]=-1
        
#cells are allocated in base layer (Nz=ri, because ri is the bottom layer in the padded array), droplet is in center of domain
while (n/float(d**2)) < fIJ: #had to convert (d**2) to float so (n/float(d**2)) can be a decimal value, otherwise python integer division will make it 0 or 1
    c1 = random.randint(ri+((Nc/2)-(d/2)),ri+(Nc/2)+((d-1)/2)) #this finds the indices that are d cells apart in the center of the Nc domain (which is in the center of the whole padded array)
    c2 = random.randint(ri+((Nc/2)-(d/2)),ri+(Nc/2)+((d-1)/2)) #is accurate for odd and even d and Nc values because "/" (python integer division)

    while c[c1,c2,ri] != 0: #ensures that the random cell chosen is empty (keeps finding random indices of a cell until cell is empty)
        c1 = random.randint(ri+((Nc/2)-(d/2)),ri+(Nc/2)+((d-1)/2))
        c2 = random.randint(ri+((Nc/2)-(d/2)),ri+(Nc/2)+((d-1)/2))

    #what kind of cell is allocated (I or J)?
    rprob = random.random() #python built in function that finds random decimal value between 0 and 1
    if rprob < fI: #given fraction or probability of I or J
        c[c1,c2,ri] = 1
        CellDict[c1,c2,ri] = 1
        n+=1
        
    else:
        c[c1,c2,ri] = 2
        CellDict[c1,c2,ri] = 2
        n+=1
        
        
#Growth of Cells
for i in np.arange(t0,tf,dt): #during time period of growth (this is a numpy version (start,stop,increment) of the python for loop)
    [x,y,z] = random.choice(CellDict.keys()) #choose a random cell that was allocated to grow
    
    #not worrying about if it is alive right now, might have to implement this later
    
    #calculate fractions (go through array in the specific neighborhood for the chosen random cell to count up the I and J cells to compute the fractions)
    nI=nJ=nT=nB=0 #have to reset to 0 so they can recount
    for i in range (x-ri,x+ri+1):
        for j in range (y-ri, y+ri+1):
            for k in range (z-ri, z+ri+1):
                nI = nI + (c[i,j,k] == 1) #number of cells of cell type 1 
                nJ = nJ + (c[i,j,k] == 2) #number of cells of cell type 2 
                nB = nB + (c[i,j,k] == -1) #total spaces beyond the edges (the padded cells)

    #calculate fractions (# of cells of a type divided by the volume of cells;deals with cells that are over the edge by substracting them from volume)            
    fI = (float(nI)/((pow((2*ri+1),3))-nB))
    fJ = (float(nJ)/((pow((2*ri+1),3))-nB))

    #calculate the growth rate for your cell
    rate = calculate_rate(fI,fJ,CellDict.get((x,y,z))) #CellDict.get((x,y,z)) goes in the dictionary and tells you the celltype (0,1,2) of the random cell, we pass it in to the function so we know which growth rate to compute (rate for I or rate for J)
    
    #Does it divide?
    rprob = random.random() #a python built in function to get a random float between 0 and 1
    if rprob < (rate*dt): #if the random prob of our cell is less than the (growth rate * time step)
        #check for closest empty site in 8 immediate grids first (no pushing)
        #if empty, place daughter cell and update array and dictionary
        #otherwise, check for empty sites within (x,y) planar confinement neighborhood of n-cell radius (r parameter)
        
        nearest_site = 0 #boolean, 1 if nearest site is found
        l=1 #gridlength, starts at 1 aka checks adjacent sites first 
        empty_sites = {} #dictionary of index and empty site found 
        
        while (nearest_site==0 and l<=r): #while no nearest site is found 
            if l==1: #do immediate sites first
                #check 4 nearest sites (the sideways sites)
                if c[x+l,y,z] == 0:
                    empty_sites[1]= [x+l,y,z]
                if c[x-l,y,z] == 0:
                    empty_sites[2]= [x-l,y,z]
                if c[x,y+l,z] == 0:
                    empty_sites[3]= [x,y+l,z]
                if c[x,y-l,z] == 0:
                    empty_sites[4]= [x,y-l,z]
                    
                if len(empty_sites) == 0: #if no sideway empty sites found, check 4 diagonals. If a sideways site is found, it won't check diagonals.
                    if c[x+l,y+l,z] == 0:
                        empty_sites[1]= [x+l,y+l,z] #1 is the key in the dictionary and [x+l,y+l,z] is the value. #1 is an index, later a random index will be chosen
                    if c[x-l,y+l,z] == 0:
                        empty_sites[2]= [x-l,y+l,z]
                    if c[x+l,y-l,z] == 0:
                        empty_sites[3]= [x+l,y-l,z]
                    if c[x-l,y-l,z] == 0:
                        empty_sites[4]= [x-l,y-l,z] 
                        
                if len(empty_sites) != 0: #if some empty site has been found from checking the immediate sites       
                    number = random.choice(empty_sites.keys()) #randomly will pick an empty site (if one of the 4 sideways is empty, it gets preference over a diagonal. If there are no empty sideways sites, it check diagonals and if there are any empty diagonals, a random diagonal will be picked).
                    [xe,ye,ze] = empty_sites.get(number) #gets the empty site based on the random index (or key in the dictionary) chosen 
                    c[xe,ye,ze] = c[x,y,z] #fill the empty spot with the cell type of cell that grew
                    CellDict[xe,ye,ze] = c[x,y,z] #update dictionary          
                    nearest_site= 1 #boolean that nearest site is found, while loop will not be repeated    
                    
                l=r #will not check immediate sites again when going through while loop if there is no empty sites found from immediate ones
                    
            else:
                index = 1 #index of empty site
                distances = {} # dictionary of empty sites and their distances to the cell that is growing 
                min_dist = {}
                #check all sites within radius that are not immediate sites
                #end up checking whole square, including 8 immediate sites (which is inefficient but might be better than multiple for loops to check all the rectangles around the inside square (or  square of immediate sites)
                for i in range (x-r,x+r+1):
                    for j in range (y-r, y+r+1):
                        if c[i,j,z] == 0:
                            empty_sites[index] = [i,j,z]
                            index +=1
                            
                if len(empty_sites) != 0: #if empty sites were found
                    #print(empty_sites)
                    for i in range (1,index+1): #for every empty site, it will retreive the location by the index (which is the key to the location value)
                        [xe,ye,ze] = empty_sites.get(i)
                        dist = math.sqrt(pow(xe-x,2)+(pow(ye-y,2))) #compute distance 
                        distances[xe,ye,ze] = dist #adds distance and location of empty site to the "distances" dictionary
                        
                    #finds and puts the min distance(s) into a min distances dictionary with the empty site as the key
                    for k in distances.keys():
                        if distances[k] == min(distances.values()):
                            min_dist[k] = distances[k] 
                    
                    if min(distances.values) < r: #is the min distance less than the radius? If yes,grow. If no,skip this clause, get out of while loop and go up
                        [xe,ye,ze] = random.choice(min_dist.keys()) #choose a random min distance site if there are multiple, or just choose the one
                        #we know we have to tracepath bc we know that all immediate sites are full (if they were empty, we even wouldnt be in this else clause)
                        TP = Tracepath(x,y,xe,ye) #call tracepath function
                        
                        #fill in things and update cell array and neighborhood
                        for i in xrange(len(TP)-1,0,-1): #backwards for loop,starts at last site (original empty site) in the TP numpy array of x,y coordinates
                            c[TP[i][0],TP[i][1],z] = c[TP[i-1][0],TP[i-1][1],z] #pushing things; update cell site with the one before it in the TP array 
                            CellDict[TP[i][0],TP[i][1],z] = c[TP[i-1][0],TP[i-1][1],z]  #update dictionary
                            
                        c[TP[0][0], TP[0][1],z] = c[x,y,z] #the first site that needs to be moved (an adjacent site) is filled with the daughter (it will be the same type as the mother)
                        nearest_site = 1 #boolean is true
                    
                l+=1 #if no empty site within n-cell radius is found, l will be greater than r and the while loop will stop 
        
        #still no nearest site within plane, go up
        zl=ri #z layer we are currently in, start at bottom (remember because of padding, ri is the bottom layer)
        while nearest_site == 0 and zl < Nz: 
            zl+=1
            if [x,y,z+zl] == 0: #found empty site
                nearest_site = 1
                while zl > ri: 
                    c[x,y,z+zl] = c[x,y,z+z1-1] #places daughter in cell above mother, if multiple cells are stacked it pushes all of them up
                    CellDict[x,y,z+z1] = c[x,y,z+z1-1] #update dictionary
                    zl -=1
                    
print("done")
print (c[:,:,ri])
#duration = default_timer() - start
#print(duration)
                    
                